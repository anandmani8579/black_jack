import React, { useEffect, useState } from "react";
import { cardsData } from "../Assets/Data/Cards";

const Board = (props) => {
  const [playerCards, setPlayerCards] = useState([]);
  const [dealerCards, setDealerCards] = useState([]);
  const [playerTotalValue, setPlayerTotalValue] = useState("");
  const [dealerTotalValue, setDealerTotalValue] = useState("");

  const getRandomCards = (num) => {
    let resultArr = Array.from(Array(num)).map(
      (x) => cardsData[Math.floor(Math.random() * cardsData.length)]
    );
    let changedArr = resultArr.map((item) => changeValue(item.faceValue));
    let total = changedArr.reduce((a, b) => a + b);
    return {
      randomArray: resultArr,
      totalVal: total,
    };
  };

  const changeShape = (shape) => {
    switch (shape) {
      case "spades":
        return "♠";
      case "diamonds":
        return "♦";
      case "clubs":
        return "♣";
      case "hearts":
        return "♥";
      default:
        return shape;
    }
  };

  const changeValue = (value) => {
    switch (value) {
      case "J":
        return 10;
      case "K":
        return 10;
      case "Q":
        return 10;
      case "A":
        return 11;
      default:
        return value;
    }
  };

  const intialCards = () => {
    let playerCardsArr = getRandomCards(2);
    let dealerCardsArr = getRandomCards(1);
    setPlayerCards(playerCardsArr.randomArray);
    setDealerCards(dealerCardsArr.randomArray);
    setPlayerTotalValue(playerCardsArr.totalVal);
    setDealerTotalValue(dealerCardsArr.totalVal);
  };

  useEffect(() => {
    intialCards();
  }, []);

  useEffect(() => {
    if (playerTotalValue > 21) {
      alert("Busted Dealer Win!");
      intialCards();
    }
  }, [playerTotalValue]);

  useEffect(() => {
    if (dealerTotalValue > 21) {
      alert("Busted Player Win!");
      intialCards();
    }
  }, [dealerTotalValue]);

  const handleHit = () => {
    if (playerTotalValue < 21) {
      let getRandom = getRandomCards(1).randomArray;
      let changedArr = getRandom.map((item) => changeValue(item.faceValue));
      setPlayerCards((prevState) => [
        ...prevState,
        { faceValue: getRandom[0].faceValue, shape: getRandom[0].shape },
      ]);
      setPlayerTotalValue((prevState) => prevState + changedArr[0]);
    } else {
      alert("busted");
    }
  };

  const handleStand = () => {
    let getRandom = getRandomCards(1).randomArray;
    let changedArr = getRandom.map((item) => changeValue(item.faceValue));
    setDealerCards((prevState) => [
      ...prevState,
      { faceValue: getRandom[0].faceValue, shape: getRandom[0].shape },
    ]);
    setDealerTotalValue((prevState) => prevState + changedArr[0]);
  };

  return (
    <div className="main-panel">
      <div className="header-panel">Black Jack</div>
      <div className="btn-wrap-de">
        <div className="player-total ml25">{dealerTotalValue}</div>
      </div>
      <div className="cards-panel">
        <div className="dealer-wrap">
          <div className="dealer-logo">D</div>
          <div className="cards-wrap">
            {dealerCards && dealerCards.length < 2 && (
              <div className="card-main blank-card">
                <div className="face-value">
                  <span></span>
                </div>
                <div className="shape"></div>
              </div>
            )}
            {Array.isArray(dealerCards) &&
              dealerCards.map((item, i) => (
                <div className="card-main" key={i}>
                  <div className="face-value">
                    <span>{item.faceValue}</span>
                  </div>
                  <div
                    className={
                      item.shape === "hearts" || item.shape === "diamonds"
                        ? "shapeRed shape"
                        : "shapeBlue shape"
                    }
                  >
                    {changeShape(item.shape)}
                  </div>
                </div>
              ))}
          </div>
        </div>
        <div className="player-wrap">
          <div className="player-logo">P</div>

          <div className="cards-wrap">
            {Array.isArray(playerCards) &&
              playerCards.map((item, i) => (
                <div className="card-main" key={i}>
                  <div className="face-value">
                    <span>{item.faceValue}</span>
                  </div>
                  <div
                    className={
                      item.shape === "hearts" || item.shape === "diamonds"
                        ? "shapeRed shape"
                        : "shapeBlue shape"
                    }
                    shape
                  >
                    {changeShape(item.shape)}
                  </div>
                </div>
              ))}
          </div>
        </div>
      </div>
      <div className="btn-wrap">
        <button onClick={handleHit}>Hit</button>
        <div className="player-total">{playerTotalValue}</div>
        <button onClick={handleStand}>Stand</button>
      </div>
    </div>
  );
};

export default Board;
