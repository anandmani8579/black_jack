export const cardsData = [
  {
    faceValue: "A",
    shape: "spades",
  },
  {
    faceValue: "A",
    shape: "diamonds",
  },
  {
    faceValue: "A",
    shape: "clubs",
  },
  {
    faceValue: "A",
    shape: "hearts",
  },
  {
    faceValue: 2,
    shape: "spades",
  },
  {
    faceValue: 2,
    shape: "diamonds",
  },
  {
    faceValue: 2,
    shape: "clubs",
  },
  {
    faceValue: 2,
    shape: "hearts",
  },
  {
    faceValue: 3,
    shape: "spades",
  },
  {
    faceValue: 3,
    shape: "diamonds",
  },
  {
    faceValue: 3,
    shape: "clubs",
  },
  {
    faceValue: 3,
    shape: "hearts",
  },
  {
    faceValue: 4,
    shape: "spades",
  },
  {
    faceValue: 4,
    shape: "diamonds",
  },
  {
    faceValue: 4,
    shape: "clubs",
  },
  {
    faceValue: 4,
    shape: "hearts",
  },
  {
    faceValue: 5,
    shape: "spades",
  },
  {
    faceValue: 5,
    shape: "diamonds",
  },
  {
    faceValue: 5,
    shape: "clubs",
  },
  {
    faceValue: 5,
    shape: "hearts",
  },
  {
    faceValue: 6,
    shape: "spades",
  },
  {
    faceValue: 6,
    shape: "diamonds",
  },
  {
    faceValue: 6,
    shape: "clubs",
  },
  {
    faceValue: 6,
    shape: "hearts",
  },
  {
    faceValue: 7,
    shape: "spades",
  },
  {
    faceValue: 7,
    shape: "diamonds",
  },
  {
    faceValue: 7,
    shape: "clubs",
  },
  {
    faceValue: 7,
    shape: "hearts",
  },
  {
    faceValue: 8,
    shape: "spades",
  },
  {
    faceValue: 8,
    shape: "diamonds",
  },
  {
    faceValue: 8,
    shape: "clubs",
  },
  {
    faceValue: 8,
    shape: "hearts",
  },
  {
    faceValue: 9,
    shape: "spades",
  },
  {
    faceValue: 9,
    shape: "diamonds",
  },
  {
    faceValue: 9,
    shape: "clubs",
  },
  {
    faceValue: 9,
    shape: "hearts",
  },
  {
    faceValue: 10,
    shape: "spades",
  },
  {
    faceValue: 10,
    shape: "diamonds",
  },
  {
    faceValue: 10,
    shape: "clubs",
  },
  {
    faceValue: 10,
    shape: "hearts",
  },
  {
    faceValue: "J",
    shape: "spades",
  },
  {
    faceValue: "J",
    shape: "diamonds",
  },
  {
    faceValue: "J",
    shape: "clubs",
  },
  {
    faceValue: "J",
    shape: "hearts",
  },
  {
    faceValue: "Q",
    shape: "spades",
  },
  {
    faceValue: "Q",
    shape: "diamonds",
  },
  {
    faceValue: "Q",
    shape: "clubs",
  },
  {
    faceValue: "Q",
    shape: "hearts",
  },
  {
    faceValue: "K",
    shape: "spades",
  },
  {
    faceValue: "K",
    shape: "diamonds",
  },
  {
    faceValue: "K",
    shape: "clubs",
  },
  {
    faceValue: "K",
    shape: "hearts",
  },
];
